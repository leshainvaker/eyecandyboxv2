<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class UserApiController extends Controller
{

    public function login(Request $request)
    {
        $password = $request->input('password');
        $email = $request->input('email');

        $user = User::where([
            'email' => $email
        ])->first();

        if(!$user) {
            return response()->json(['error' => 'user not found'], 404);
        }

        if (!Hash::check($password, $user->password))  {
            return response()->json(['error' => 'user not found'], 404);
        }

        $user->api_token = str_random(60);
        $user->save();

        return response()->json(['api_token' => $user->api_token], 201);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(5);
        return response()->json($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            array(
                'name' => $request->input('name'),
                'password' => $request->input('password'),
                'email' => $request->input('email')
            ),
            array(
                'name' => 'required|max:255',
                'password' => 'required|min:6',
                'email' => 'required|email|max:255|unique:users'
            )
        );


        if ($validator->fails())
        {
            return response()->json(['data' => $validator->messages(), 'status' => 'error'], 404);
        }

        $name = $request->input('name');
        $password = $request->input('password');
        $email = $request->input('email');

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'api_token' => str_random(60),
        ]);

        return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if(!$user) {
            return response()->json(['error' => 'user not found'], 404);
        }
        return response()->json($user, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::find(Auth::id());
        if($request->hasFile('avatar')) {
            $request->file('avatar')->store('images');
        }
        if($request->hasFile('banner')) {
            $request->file('banner')->store('images');
        }
        $check = $user->fill($request->all())->save();
        if(!$check) {
            return response()->json(['error' => 'smt wrong'], 404);
        }
        return response()->json(['ok' => 'User has been updated'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $user = User::find(Auth::id());
        $user->delete();

        return response()->json(['ok' => 'User has been deleted'], 200);
    }
}
