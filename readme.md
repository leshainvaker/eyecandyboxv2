# INFO

## Requirements

### Laravel 5.3

* PHP >= 5.6.4
* Composer
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

## Installation

1. ``` git clone https://leshainvaker@bitbucket.org/leshainvaker/eyecandyboxv2.git ```
2. ``` copmoser install ```
3. create .env file
4. ``` php artisan key:gen ```
5. ``` php artisan migrate ```
6. for test db ``` php artisan db:seed ```

test login: ``` test@test.com/qwerty ```

## API docs

[API](docs/index.md)