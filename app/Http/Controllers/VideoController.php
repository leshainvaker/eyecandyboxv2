<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::paginate(5);
        return response()->json($videos, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            [
                'title' => $request->input('title'),
                'description' => $request->input('description'),
            ],
            [
                'title' => 'required|max:50',
                'description' => 'required|max:255',
            ]
        );


        if ($validator->fails())
        {
            return response()->json(['data' => $validator->messages(), 'status' => 'error'], 404);
        }

        $title = $request->input('title');

        $video = Video::create([
            'title' => $title,
        ]);

        return response()->json([$video], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::find($id);

        if(!$video) {
            return response()->json(['error' => 'video not found'], 404);
        }
        return response()->json($video, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
