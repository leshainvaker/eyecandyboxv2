<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'v1'], function() {
    Route::post('/registration', 'UserApiController@store');
    Route::post('/login', 'UserApiController@login');
});

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {
    Route::get('/user', function() {
        return Auth::user();
    });

    Route::delete('/users/delete', 'UserApiController@destroy');
    Route::put('/users/update', 'UserApiController@update');
    
    Route::resource('users', 'UserApiController', ['only' => ['index', 'show']]);
    Route::resource('videos', 'VideoController', ['only' => ['index', 'show', 'update', 'destroy', 'store']]);
    Route::resource('categories', 'CategoryController', ['only' => ['index', 'show', 'update', 'destroy', 'store']]);
});
