# API Documentation

url - domain.com/api/v1

##User

### User Registration

#### POST /registration

Headers:
```
Content-Type: application/json
```
Request:

```
{
	"name":"Alex",
	"email":"example@gmail.com",
	"password":"qwerty"
}
```

Response:

ok Status 201
```
{

  "api_token": "API_TOKEN"

}
```

error Status 404
```
{
  "data": {
    "name": [
      "The name field is required."
    ],
    "password": [
      "The password must be at least 6 characters."
    ],
    "email": [
      "The email has already been taken."
    ]
  },
  "status": "error"
}
```

### User Login

#### POST /login

Headers:
```
Content-Type: application/json
Accept: application/json
```
Request:

```
{
    "email":"email@gmail.com",
    "password":"qwerty"
}
```

Response:

ok Status 201
```
{

  "api_token": "API_TOKEN"

}
```

error Status 404
```
{
  "error": "user not found"
}
```

### Get All Users

#### GET /users

Headers:
```
Content-Type: application/json
Authorization: Bearer API_TOKEN
```

Response:

ok Status 200
```
{
  "total": 11,
  "per_page": 5,
  "current_page": 1,
  "last_page": 4,
  "next_page_url": "http://domain.com/api/v1/users?page=2",
  "prev_page_url": null,
  "from": 1,
  "to": 3,
  "data": [
    {
      "id": 1,
      "name": "User1",
      "email": "example1@gmail.com",
      "avatar": "default.jpg",
      "banner": "default-banner.jpg",
      "created_at": "2017-03-03 09:01:14",
      "updated_at": "2017-03-03 09:01:14"
    },
    {
      "id": 2,
      "name": "User2",
      "email": "example2@gmail.com",
      "avatar": "default.jpg",
      "banner": "default-banner.jpg",
      "created_at": "2017-03-03 15:51:45",
      "updated_at": "2017-03-03 15:51:45"
    },
    {
      "id": N,
      "name": "UserN",
      "email": "exampleN@test1.com",
      "avatar": "default.jpg",
      "banner": "default-banner.jpg",
      "created_at": "2017-03-03 15:52:35",
      "updated_at": "2017-03-03 15:52:35"
    }
  ]
}
```

### Get User

#### GET /users/{id}

Headers:
```
Content-Type: application/json
Authorization: Bearer API_TOKEN
```

Response:

ok Status 200
```
{
  "id": 1,
  "name": "Alex Tregubov",
  "email": "tgnwest.work@gmail.com",
  "avatar": "https://graph.facebook.com/v2.8/106777363184469/picture?type=normal",
  "banner": "default-banner.jpg",
  "created_at": "2017-03-03 09:01:14",
  "updated_at": "2017-03-03 09:01:14"
}
```

error Status 404
```
{
  "error": "user not found"
}
```

### Delete User

#### DELETE /users/delete

Headers:
```
Content-Type: application/json
Accept: application/json
Authorization: Bearer API_TOKEN
```

Response:

ok Status 200
```
{
  "ok": "User has been deleted"
}
```

error Status 404
```
{
  "error": "error message"
}
```

### User Update

#### POST /users/update

Headers:
```
Content-Type: application/json
Accept: application/json
Authorization: Bearer API_TOKEN
```

Request:

```
{
    "email":"email@gmail.com",
    "name":"qwerty"
}
```
For upd avatar or banner:

Send files with name 'avatar', 'banner'

Response:

ok Status 201
```
{

  "ok": "User has been updated"

}
```

error Status 404
```
{
  "error": "smt wrong"
}
```