<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '192903297862811',
        'client_secret' => 'a78d1361713fe838ecf57050e0e32ade',
        'redirect' => 'http://eyecandyboxv2.loc/login/facebook/callback',
    ],
    'google' => [
        'client_id' => '16433280009-uqgr5j5o518jbs4roqo40cvi4es8i0ip.apps.googleusercontent.com', // configure with your app id
        'client_secret' => '7u5pF1Z_wfQXh9E8c3VO4eKK', // your app secret
        'redirect' => 'http://localhost/login/google/callback', // leave blank for now
    ],


];
